#from pybrain.structure import LinearLayer, SigmoidLayer
#from pybrain.structure import FullConnection
#from pybrain.structure.networks import feedforward as ff
import numpy as np
import VisualiseSkeleton
import LoadFile
import Utilities as U
#from pybrain.datasets            import ClassificationDataSet
#from pybrain.utilities           import percentError
#from pybrain.tools.shortcuts     import buildNetwork
#from pybrain.supervised.trainers import BackpropTrainer
#from pybrain.structure.modules   import SoftmaxLayer
#from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot
#from scipy import diag, arange, meshgrid, where
#from numpy.random import multivariate_normal


'''
means = [(-1,0),(2,4),(3,1)]
cov = [diag([1,1]), diag([0.5,1.2]), diag([1.5,0.7])]
alldata = ClassificationDataSet(2, 1, nb_classes=3)
for n in xrange(400):
    for klass in range(3):
        input = multivariate_normal(means[klass],cov[klass])
        alldata.addSample(input, [klass])
'''

x = []
y = []
for i in range(1,13):
    x.append(LoadFile.LoadFile("P1_1_"+str(i)+"_p06.csv"))
    y.append(LoadFile.LoadSep("P1_1_"+str(i)+"_p06.sep"))

data = U.SeparateSequence(x, y)
data = U.RemoveJoints(data,[0,1,2,3,4,5,6,8,9,10,12,13,14,16,17,18])

# My own recorded data
#a = np.loadtxt(open("kinectData.dat","rb"),delimiter=",")
#sep = np.loadtxt(open("kinectData.sep","rb"),delimiter=" ")
#data = [a[row[0]:row[1]] for row in sep]
#data = U.RemoveJoints(data,[0,6,7,8])

VisualiseSkeleton.VisualiseSkeleton(data) #[data[i] for i in range(9)])


'''
n = ff.FeedForwardNetwork()
inLayer = LinearLayer(2)
hiddenLayer = SigmoidLayer(3)
outLayer = LinearLayer(1)

n.addInputModule(inLayer)
n.addModule(hiddenLayer)
n.addOutputModule(outLayer)

in_to_hidden = FullConnection(inLayer, hiddenLayer)
hidden_to_out = FullConnection(hiddenLayer, outLayer)

n.addConnection(in_to_hidden)
n.addConnection(hidden_to_out)

n.sortModules()
i = n.activate([1, 2])
print(i)
while True:
    i = n.activate([i, i])
    print(i)
'''