__author__ = 'Joe'
# coding=utf-8
from itertools import chain
import numpy as np
from scipy.special import expit

'''
Helper class
'''

# Slice a 2D array of multiple actions into a list of 2D arrays containing the individual actions
def SeparateSequence(skeletonData, seps):
    data = []
    for i, item in enumerate(skeletonData):
        for row in seps[i]:
            data.append(item[row[0]+20:row[1]+20])
    return data

# Turn an array of action sequences into one big sequence - used for VisualiseSkeleton
def ConcatenateList(data):
    tData = []
    for element in data:
        tData.extend(element)
    return tData

# Remove joints from action sequences
def RemoveJoints(data, jointsToKeep):
    jointsToKeep = list(chain.from_iterable(((i*3),(i*3)+1,(i*3)+2) for i in jointsToKeep))
    return [[[frame[i] for i in jointsToKeep] for frame in sequence]for sequence in data]

# Return unit direction vectors between connecting joints. Used as features for classification
def GetUnitDirectionVectors(data, joints):
    connections = [[1,12,16],[2],[3,4,8],[],[5],[6],[],[],[9],[10],[],[],[13],[14],[],[],[17],[18],[],[]]
    return [[[item for j,joint in enumerate(joints) for con in connections[joint] if con in joints for item in VUnitVector([frame[j*3],frame[j*3+1],frame[j*3+2]], [frame[joints.index(con)*3],frame[joints.index(con)*3+1],frame[joints.index(con)*3+2]])] for frame in seq] for seq in data]

def Dot(a, b):
    return np.dot(a,b)

def Dot3(a, b, c):
    return np.sum(np.multiply(np.multiply(a,b),c))

def SumV(a):
    return np.sum(a)

def EMultiplication(a, b):
    return np.multiply(a,b)

def EAddition(a,b):
    return np.add(a,b)

def ESubtraction(a,b):
    return np.subtract(a,b)

def Sigmoid(a):
    return expit(a)

def SigmoidDerivative(a):
    return (1 / (1 + np.exp(-a)))*(1-(1 / (1 + np.exp(-a))))

def SMultiplication(a, b):
    return a*b

def SvsMultiplication(a, b, c):
    return np.multiply(np.multiply(a, b),c)

def SssMultiplication(a, b, c):
    return a*b*c

def SvAddition(a, b):
    return np.add(a, b)

def SAddition(a, b):
    return a+b

def VUnitVector(a, b):
    sub = np.subtract(b, a)
    return sub/np.sqrt(np.dot(sub,sub))
