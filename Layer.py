# coding=utf-8
import numpy as np

class Layer:
    """ Details of the layer """

    depth = None            # Depth/Index of layer in the network
    numOfUnits = None       # No. of units in the layer
    lType = None            # Layer type e.g. input/hidden/memory/output
    unitActivations = []    # Array of unit activations, index indicates the unit
    unitStates = []         # Sj State of a unit before it has been squashed by sigmoid function
    previousUnitStates = [] # hat Sj required for calculating the extended elig traces see (B.51) in paper
    peepholeWeights = []    # Each gate layer receives the state of the memory cell (unitStates) which must be weighted against (peepholeWeights) when calculating the gate layer activations TODO determine how peephole weights are updated
    connectionWeights = []  # 2d Array of connections going into this layer, first dimension indicates the unit and second dimension indicates connection going into that unit
    upstreamUnits = None    # Holds upstream units that project connections to units in this layer e.g. unit zero in the upstreamLayer connects to unit one in this layer
    upstreamLayer = None    # Layer index that feeds into this layer
    downstreamUnits = None  # Holds downstream units that receive connections from units in this layer
    memCellIndex = None     # Layer index of memoryCell. Used for layers memInput/memForget/memOutput when calculating activations using the memCell state
    eligibilityTraces = []  # Record of activations that have crossed a connection into each unit in the layer. Eji
    extendedEligibilityTraces = []  # Record of activations that have crossed a gated connection
    deltas = []             # Error responsibility of each unit (δj) in the layer, required to adjust the weights of incoming connections. See paper 3.2 and Appendix A.1
    deltaProjects = []
    deltaGates = []

    def __init__(self, depth, numOfUnits, lType, downstreamUnits, upstreamUnits, weights, upstreamLayer=None, peepholeWeights=None):
        self.depth = depth
        self.numOfUnits = numOfUnits
        self.lType = lType
        self.connectionWeights = weights
        self.downstreamUnits = downstreamUnits
        self.upstreamUnits = upstreamUnits
        self.peepholeWeights = peepholeWeights
        if lType is not 'input':
            # Setup initial values
            self.ResetLayer()

        # Set upstreamLayer
        if upstreamLayer is None:
            self.upstreamLayer = depth-1
        else:
            self.upstreamLayer = upstreamLayer
        # Set the index of the memCell these gates refer to
        if lType is 'memForget':
            self.memCellIndex = depth+1
        elif lType is 'memInput':
            self.memCellIndex = depth+2
        elif lType is 'memOutput':
            self.memCellIndex = depth-1


    def ResetLayer(self):
        self.deltas = [0] * self.numOfUnits
        # Set dimensions
        self.eligibilityTraces = [[[0 for _ in range(len(self.upstreamUnits[i]))]] for i in range(self.numOfUnits)]
        # If has extended elig traces then set dimensions and pop values
        if self.lType is 'memForget' or self.lType is 'memInput':
            self.extendedEligibilityTraces = [[[0 for _ in range(len(self.upstreamUnits[i]))]] for i in range(self.numOfUnits)]

        if self.lType is 'memCell':
            self.unitStates = [0] * self.numOfUnits
            self.previousUnitStates = [0] * self.numOfUnits
        elif self.lType is 'memForget':
            self.unitActivations = [0] * self.numOfUnits
            self.unitStates = [0] * self.numOfUnits
            self.previousUnitStates = [0] * self.numOfUnits
        elif self.lType is 'memInput':
            self.unitStates = [0] * self.numOfUnits
            self.previousUnitStates = [0] * self.numOfUnits
        elif self.lType is 'memOutput':
            self.unitStates = [0] * self.numOfUnits
            self.previousUnitStates = [0] * self.numOfUnits