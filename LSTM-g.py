# coding=utf-8
import random
from Network import *
#from GUImain import *
import LoadFile
from itertools import chain

epochs = 1000000

f = open('weights/weights.txt','rb')
# params = numOfUnits, lType, downstreamUnits, weights, upstreamLayer, peepholeWeights=None, upstreamUnits=[]
#network = Network([9,27,2], ['input', 'memory', 'output'], [[range(27) for _ in range(9)], [range(2) for _ in range(27)], None], None, [None, 0, 3])
network = Network([45, 45, 45, 12], ['input', 'memoryNO', 'memory', 'output'], [[range(45) for _ in range(45)],[range(45) for _ in range(45)], [range(12) for _ in range(45)],  None], None, [None, 0, 3, 6])
f.close()

x = []
y = []
labels = [0]
for i in range(1,13):
    x.append(LoadFile.LoadFile("P1_1_"+str(i)+"_p06.csv"))
    y.append(LoadFile.LoadSep("P1_1_"+str(i)+"_p06.sep"))
    if i > 1:
        labels.append(labels[i-1]+LoadFile.LoadNumTags("P1_1_"+str(i)+"_p06.tagstream"))
    else:
        labels.append(LoadFile.LoadNumTags("P1_1_"+str(i)+"_p06.tagstream"))

data = U.SeparateSequence(x, y)
data = U.RemoveJoints(data,[0,1,2,3,4,5,6,8,9,10,12,13,14,16,17,18])
data = U.GetUnitDirectionVectors(data, [0,1,2,3,4,5,6,8,9,10,12,13,14,16,17,18])
labels.pop()
labels = [[1 if j < len(labels)-1 and seq >= labels[j] and seq < labels[j+1] or j == len(labels)-1 and seq >= labels[j] else 0 for j in range(len(labels))] for seq in range(len(data))]

'''
a = np.loadtxt(open("kinectData.dat","rb"),delimiter=",")
sep = np.loadtxt(open("kinectData.sep","rb"),delimiter=" ")
data = [a[row[0]:row[1]] for row in sep]
data = U.RemoveJoints(data,[0,6,7,8])
data = U.GetUnitDirectionVectors(data)
'''

# TODO Performance very slow need to: vectorise implementation/test on GPU/use single precision floats/test numpy arrays/write code to C/distributed computing (SLI api CUDA (be careful may be slower!))/Change architecture e.g. local connectivity for input layer etc
# TODO write cross validation to minimise global error ensuring it generalises to unseen examples, using 70/30
# TODO normalise inputs and use f(x) = 1.7159*tanh((2/3)*x) f'(x) = 1.14393 * (1- tanh( 2/3 * x)) (An approximation of tanh (e^x-e^(-x))/(e^x+e^(-x)) and less computationally expensive) see paper Efficient Backprop sec 4.4
# TODO Test modular neural network arch to minimise catastrophic forgetting. See paper Neural Modularity
# TODO adaptive learning rate. Learning rate decreases as the target - actual output is minimised.
#network.TrainNetwork(np.asarray([data[i] for i in [0,1,2,3,4,5,6,7,8,9,10,11,12,13]]),list(chain([[1,0] for _ in range(8)],[[0,1] for _ in range(9)])),epochs) #[data[i] for i in [0,9]]
#network.TrainNetwork(np.asarray([[[0.25,0.5,0.75]]]),[[1,0,0]],epochs) #[[0.75,0.5,0.25]]] [0,1,0]
#network.TrainNetwork(data,labels,epochs)

network.Classify(data[117], False, True)
print network.GetOutput()

# GUI is WIP
#GUI(network)








