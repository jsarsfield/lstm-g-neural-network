from Tkinter import *
from Network import Network

class GUI:
    master = Tk()
    def __init__(self, network):
        w = Canvas(self.master,width=1000, height=1000, bg="grey")
        w.pack()

        #w.create_line(0, 0, 200, 100)
        #w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
        nLayers = len(network.layers)
        midPoint = 0
        offSet = 25
        unitSize = 50
        distBetweenLayers = 100
        distBetweenUnits = 100
        for i, layer in enumerate(network.layers):
            if layer.numOfUnits > midPoint:
                midPoint = layer.numOfUnits
        midPoint = ((float(midPoint)-1)/2)*distBetweenUnits
        for i, layer in enumerate(network.layers):
            start = midPoint - (((layer.numOfUnits-1)*distBetweenUnits)/2)
            yStart = (nLayers*distBetweenLayers)-(i*distBetweenLayers)
            yFinish = yStart+unitSize
            for j, ua in enumerate(layer.unitActivations):
                w.create_oval(start+(j*distBetweenUnits), yStart, start+(j*distBetweenUnits)+unitSize, yFinish, fill="white")
                w.create_text(start+(j*distBetweenUnits)+(unitSize/2),yStart+(unitSize/2),text=round(ua, 4))
                if j is len(layer.unitActivations)-1 and layer.connectionWeights is not None:
                    w.create_text((midPoint*2)+150,yStart+(unitSize/2),text=[" " + '%.4f' % item for sublist in layer.connectionWeights for item in sublist])

        mainloop()