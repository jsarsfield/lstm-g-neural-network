__author__ = 'Joe'
'''
Unit tests for Network class
'''

from Network import Network
from mock import patch
import unittest
import numpy as np
import Utilities as U

class NetworkTest(unittest.TestCase):
    network = Network( [3,3,3], ['input', 'memory', 'output'], [[range(3) for _ in range(3)], [range(3) for _ in range(3)], None], [None,[[1,2,3],[1,2,3],[1,2,3]],[[1,2,3],[1,2,3],[1,2,3]],[[1,2,3],[1,2,3],[1,2,3]],[[1,2,3],[1,2,3],[1,2,3]],[[1,2,3],[1,2,3],[1,2,3]]], [None, 0, 3])

    # Is there 3 units in each layer
    def test_NumberOfUnits(self):
        assert len([True for i,layer in enumerate(self.network.layers) if layer.numOfUnits == 3]) == 6

    network.Classify([[1,2,3]], False, False)

    # Test network classification outputs
    def test_NetworkClassifyOutputs(self):
        np.testing.assert_allclose(self.network.GetOutput(), np.array([0.997527364537,0.997527364537,0.997527364537]))

    def test_ComputeMemCellActivation(self):
        np.testing.assert_allclose(self.network.layers[3].unitActivations[0], np.array([ 0.99999917, 0.99999917, 0.99999917]))

    def test_ComputeMemGatedActivation(self):
        np.testing.assert_allclose(self.network.layers[5].unitActivations[0], np.array([ 0.99752736, 0.99752736, 0.99752736]))


class UtilitiesTest(unittest.TestCase):

    def test_dot(self):
        assert U.Dot([1,2,3],[1,2,3]) == 14

    def test_dot3(self):
        assert U.Dot3([1,2,3],[1,2,3],[1,2,3]) == 36

    def test_sumV(self):
        assert U.SumV([1,2,3]) == 6

    def test_eMultiplication(self):
        assert U.EMultiplication([1,2,3],[1,2,3]).tolist() == [1,4,9]
        assert U.EMultiplication(2,[1,2,3]).tolist() == [2,4,6]

    def test_eAddition(self):
        assert U.EAddition([1,2,3],[1,2,3]).tolist() == [2,4,6]

    def test_eSubtraction(self):
        assert U.ESubtraction([1,2,3],[1,2,3]).tolist() == [0,0,0]

    def test_sigmoid(self):
        np.testing.assert_allclose(U.Sigmoid(0.5), 0.622459331202)

    def test_SigmoidDerivative(self):
        np.testing.assert_allclose(U.SigmoidDerivative(0.6), 0.228784240456)

    def test_sMultiplication(self):
        assert U.SMultiplication(2,3) == 6

    def test_svsMultiplication(self):
        assert U.SvsMultiplication(2,[1,2,3],3).tolist() == [6,12,18]

    def test_sssMultiplication(self):
        assert U.SssMultiplication(2,3,4) == 24

    def test_svAddition(self):
        assert U.SvAddition(2,[1,2,3]).tolist() == [3,4,5]

    def test_sAddition(self):
        assert U.SAddition(2,3) == 5

    def test_vUnitVector(self):
        np.testing.assert_allclose(U.VUnitVector([1,2,3],[3,2,1]), [0.707106781187,0,-0.707106781187])
