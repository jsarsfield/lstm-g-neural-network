# coding=utf-8
import timeit
from Layer import *
import numpy as np
import Utilities as U
import random as R
import pickle
from Input import *
import threading

class Network:
    """ Defines a neural network architecture. """

    layers = []         # Layers in the network in order of depth, input first output last
    timeStep = 0        # Stores current timestep of network
    #TODO time decaying learning rate to speed up training but also allow the network to converge
    learningRate = 0.1  # α learning rate of back propagation algorithm. Use smaller values if failing to converge or larger values if training is taking too long

    # Layers is a 2D array containing layer depth, number of units and layer type
    # e.g. [(0, 2, 'input'),(1, 3, 'hidden'), (2, 1, 'output')]
    # Layer types 'input' 'hidden' 'memory' 'output'
    def __init__(self, numOfUnits, lType, downstreamUnits, weights, upstreamLayer, peepholeWeights=None, upstreamUnits=[]):

        # Calculate upstreamUnits based off downstreamUnits if not supplied
        if len(upstreamUnits) is 0:
            upstreamUnits = [None] + [[[us for us in range(numOfUnits[l]) for u in range(len(downstreamUnits[l][us])) if downstreamUnits[l][us][u] == uu] for uu in range(numOfUnits[l+1])] for l in range(len(downstreamUnits)) if downstreamUnits[l] is not None]

        # If no weights supplied then randomly generate them
        if weights is None:
            weights = []
            for i in range(len(lType)):
                if i is 0:
                    weights.append(None)
                # Create a list of random numbers between 0-1 for the weights. 4 times the amount of numbers for memory blocks as they have 4 layers.
                elif lType[i] == 'memory':
                    [weights.append([[R.random() for _ in enumerate(m)] for m in upstreamUnits[i]]) for _ in xrange(4)]
                elif lType[i] == 'memoryNO':
                    [weights.append([[R.random() for _ in enumerate(m)] for m in upstreamUnits[i]]) for _ in xrange(3)]
                else:
                    weights.append([[R.random() for _ in enumerate(m)] for m in upstreamUnits[i]])
        j = 0
        for i in range(len(lType)):  # np.array(layers, dtype='i4, i4, S8, |i4, |f32'):
            if peepholeWeights is None and 'memory' in lType[i]:
                peepholeWeights = [[1 for _ in xrange(numOfUnits[i])] for _ in xrange(3)]
            if 'memory' in lType[i]:
                self.layers.append(Layer(j, numOfUnits[i], 'memInput', downstreamUnits[i], upstreamUnits[i], weights[j], upstreamLayer[i], peepholeWeights=peepholeWeights[0]))
                self.layers.append(Layer(j+1, numOfUnits[i], 'memForget', downstreamUnits[i], upstreamUnits[i], weights[j+1], upstreamLayer[i], peepholeWeights=peepholeWeights[1]))
                self.layers.append(Layer(j+2, numOfUnits[i], 'memCell', downstreamUnits[i], upstreamUnits[i], weights[j+2], upstreamLayer[i]))
                if 'NO' not in lType[i]:
                    self.layers.append(Layer(j+3, numOfUnits[i], 'memOutput', downstreamUnits[i], upstreamUnits[i], weights[j+3], upstreamLayer[i], peepholeWeights=peepholeWeights[2]))
                    j += 3
                else:
                    j += 2
            else:
                self.layers.append(Layer(j, numOfUnits[i], lType[i], downstreamUnits[i], upstreamUnits[i], weights[j], upstreamLayer[i]))
            j += 1

    # Compute activation of memory cell
    def ComputeMemCellActivation(self, upstreamUnits, ind, unitInd):
        self.layers[ind].unitStates[unitInd] = U.SAddition(U.SMultiplication(self.layers[ind-1].unitActivations[unitInd],self.layers[ind].previousUnitStates[unitInd]),
                                                      U.SMultiplication(self.layers[ind-2].unitActivations[unitInd],U.Dot(self.layers[ind].connectionWeights[unitInd],self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits])))
        self.layers[ind].unitActivations.append(U.Sigmoid(self.layers[ind].unitStates[unitInd]))

    # Compute the activation of a unit in a gate layer memInput/memForget/memOutput
    def ComputeGateActivation(self, upstreamUnits, ind, unitInd):
        self.layers[ind].unitStates[unitInd] = U.SAddition(U.SMultiplication(self.layers[self.layers[ind].memCellIndex].unitStates[unitInd], self.layers[ind].peepholeWeights[unitInd]),
                                               U.Dot(self.layers[ind].connectionWeights[unitInd],  self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits]))
        self.layers[ind].unitActivations.append(U.Sigmoid(self.layers[ind].unitStates[unitInd]))

    # Compute the activation of a unit gated by a memOuput unit
    def ComputeMemGatedActivation(self, upstreamUnits, ind, unitInd):
        self.layers[ind].unitActivations.append(U.Sigmoid(U.Dot3(self.layers[ind-1].unitActivations[upstreamUnits],
                                                                     self.layers[ind].connectionWeights[unitInd],
                                                                     self.layers[ind-2].unitActivations[upstreamUnits])))

    # Compute activation of a normal unit sum(weights*units) all to all connectivity
    def ComputeUnitActivation(self, upstreamUnits, ind, unitInd):
        self.layers[ind].unitActivations.append(U.Sigmoid(U.Dot(self.layers[ind].connectionWeights[unitInd],
                                                                self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits])))

    # Compute elig traces for memCell (Eq. 17)
    def ComputeMemCellTraces(self, upstreamUnits, ind, unitInd):
        self.layers[ind].eligibilityTraces[unitInd].insert(0,U.EAddition(U.EMultiplication(self.layers[ind-1].unitActivations[unitInd], self.layers[ind].eligibilityTraces[unitInd][0]),
                                                                         U.EMultiplication(self.layers[ind-2].unitActivations[unitInd], self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits])))
    # Compute elig traces for unit connections gated by memOutput. gji yi
    def ComputeMemGatedTraces(self, upstreamUnits, ind, unitInd):
        self.layers[ind].eligibilityTraces[unitInd].insert(0,U.EMultiplication(self.layers[ind-1].unitActivations[upstreamUnits], self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits]))
    # Compute elig traces for normal unit. yi
    def ComputeUnitTraces(self, upstreamUnits, ind, unitInd):
        self.layers[ind].eligibilityTraces[unitInd].insert(0,self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits])
        # self.layers[ind].eligibilityTraces[unitInd].pop() TODO pop previous elig values we no longer need for updating weights
    # Compute the extended eligibility traces of the forget gate layer
    def ComputeExMemForgetTraces(self, upstreamUnits, ind, unitInd):
        # TODO check B.51 and B.64 for the first timestep when previous extended elig traces do not exist DOES forget gate activations cancel out
        self.layers[ind].extendedEligibilityTraces[unitInd].insert(0,U.EAddition(U.EMultiplication(self.layers[ind].unitActivations[unitInd],
                                                                                                    self.layers[ind].extendedEligibilityTraces[unitInd][0]),
                                                                   U.SvsMultiplication(U.SigmoidDerivative(self.layers[ind].unitStates[unitInd]),
                                                                                       self.layers[ind].eligibilityTraces[unitInd][0],
                                                                                       self.layers[self.layers[ind].memCellIndex].previousUnitStates[unitInd])))
    # Compute the extended eligibility traces of the input gate layer
    def ComputeExMemInputTraces(self, upstreamUnits, ind, unitInd):
        # TODO check B.51 and B.64 for the first timestep when previous extended elig traces do not exist DOES forget gate activations cancel out
        self.layers[ind].extendedEligibilityTraces[unitInd].insert(0,U.EAddition(U.EMultiplication(self.layers[ind+1].unitActivations[unitInd],
                                                                                                    self.layers[ind].extendedEligibilityTraces[unitInd][0]),
                                                                   U.SvsMultiplication(U.SigmoidDerivative(self.layers[ind].unitStates[unitInd]),
                                                                                       self.layers[ind].eligibilityTraces[unitInd][0],
                                                                                       U.Dot(self.layers[ind].connectionWeights[unitInd],
                                                                                             self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits]))))
    # Calculate delta for output layer (Eq. 10)
    def ComputeDeltaOutput(self, targets, ind):
        self.layers[ind].deltas = U.ESubtraction(targets,self.layers[ind].unitActivations)
        # Adaptive learning rate
        self.learningRate = (sum([abs(targets[i]-item) for i,item in enumerate(self.layers[ind].unitActivations)]) * 0.01)+0.01

    # Calculate delta for output gate layer (Eq. 22)
    def ComputeDeltaOutputGate(self, ind, unitInd):
        self.layers[ind].deltas[unitInd] = U.SssMultiplication(U.SigmoidDerivative(self.layers[ind].unitStates[unitInd]),
                                                      U.SumV([self.layers[ind+1].deltas[i] for i in self.layers[ind].downstreamUnits[unitInd]]),
                                                      U.SumV(U.EMultiplication(self.layers[ind-1].unitActivations[unitInd],[self.layers[ind+1].connectionWeights[i][j] for i in self.layers[ind-1].downstreamUnits[unitInd] for j, v in enumerate(self.layers[ind+1].upstreamUnits[i]) if v == unitInd])))

    # Calculate delta for memCell layer (Eq. 21, B.39)
    def ComputeDeltaMemCell(self, ind, unitInd):
        if self.layers[ind+1].lType == "memOutput":
            self.layers[ind].deltas[unitInd] = U.SMultiplication(U.SigmoidDerivative(self.layers[ind].unitStates[unitInd]),
                                                                   U.SAddition(U.SMultiplication(self.layers[ind+1].unitActivations[unitInd],U.Dot([self.layers[ind+2].deltas[i] for i in self.layers[ind].downstreamUnits[unitInd]],
                                                                                                          [self.layers[ind+2].connectionWeights[i][j] for i in self.layers[ind].downstreamUnits[unitInd] for j, v in enumerate(self.layers[ind+2].upstreamUnits[i]) if v == unitInd])),
                                                                               U.SMultiplication(self.layers[ind+1].deltas[unitInd],self.layers[ind+1].peepholeWeights[unitInd])))
        else:
            self.layers[ind].deltas[unitInd] = U.SMultiplication(U.SigmoidDerivative(self.layers[ind].unitStates[unitInd]),
                                                                   U.Dot3([self.layers[ind+3].deltas[i] for i in self.layers[ind].downstreamUnits[unitInd]],
                                                                   [self.layers[ind+1].unitActivations[i] for i in self.layers[ind].downstreamUnits[unitInd]],
                                                                   [self.layers[ind+3].connectionWeights[i][j] for i in self.layers[ind].downstreamUnits[unitInd] for j, v in enumerate(self.layers[ind+3].upstreamUnits[i]) if v == unitInd]))
    # Calculate delta for memForget layer
    def ComputeDeltaForgetGate(self, ind, unitInd):
        self.layers[ind].deltas[unitInd] = U.SMultiplication(U.SigmoidDerivative(self.layers[ind].unitStates[unitInd]),
                                                             U.SMultiplication(self.layers[ind+1].deltas[unitInd],self.layers[ind+1].previousUnitStates[unitInd]))

    # Calculate delta for memInput layer
    def ComputeDeltaInputGate(self, upstreamUnits, ind, unitInd):
        self.layers[ind].deltas[unitInd] = U.SMultiplication(U.SigmoidDerivative(self.layers[ind].unitStates[unitInd]),
                                                             U.SMultiplication(self.layers[ind+2].deltas[unitInd],
                                                                               U.Dot(self.layers[ind+2].connectionWeights[unitInd],self.layers[self.layers[ind].upstreamLayer].unitActivations[upstreamUnits])))
    # Update weights for output layer
    def UpdateOutputWeights(self, ind, unitInd):
        self.layers[ind].connectionWeights[unitInd] = U.EAddition(self.layers[ind].connectionWeights[unitInd],
                                                                  U.EMultiplication(self.learningRate, U.EMultiplication(self.layers[ind].deltas[unitInd],self.layers[ind].eligibilityTraces[unitInd][0])))

    # Update weights for output gate layer
    def UpdateMemOutputWeights(self, ind, unitInd):
        self.layers[ind].connectionWeights[unitInd] = U.EAddition(self.layers[ind].connectionWeights[unitInd],
                                                                  U.EMultiplication(self.learningRate, U.EMultiplication(self.layers[ind].deltas[unitInd],self.layers[ind].eligibilityTraces[unitInd][0])))

    # Update weights for memory cell layer
    def UpdateMemCellWeights(self, ind, unitInd):
        self.layers[ind].connectionWeights[unitInd] = U.EAddition(self.layers[ind].connectionWeights[unitInd],
                                                                  U.EMultiplication(self.learningRate, U.EMultiplication(self.layers[ind].deltas[unitInd],self.layers[ind].eligibilityTraces[unitInd][0])))

    # Update weights for forget gate layer
    def UpdateMemForgetWeights(self, ind, unitInd):
        self.layers[ind].connectionWeights[unitInd] = U.EAddition(self.layers[ind].connectionWeights[unitInd],
                                                                  U.EMultiplication(self.learningRate, U.EMultiplication(self.layers[ind+1].deltas[unitInd],self.layers[ind].extendedEligibilityTraces[unitInd][0])))

    # Update weights for input gate layer
    def UpdateMemInputWeights(self, ind, unitInd):
        self.layers[ind].connectionWeights[unitInd] = U.EAddition(self.layers[ind].connectionWeights[unitInd],
                                                                  U.EMultiplication(self.learningRate, U.EMultiplication(self.layers[ind+2].deltas[unitInd],self.layers[ind].extendedEligibilityTraces[unitInd][0])))

    # Iterate over all examples in the TrainingData the number of times expressed in epochs
    def TrainNetwork(self, trainingData, labelData, epochs):
        kb = KBHit()
        # Train the NN with the full training set this many times
        # TODO randomise the order of the training samples each epoch as this will reduce the chances of getting stuck in local minima with online learning
        for y in range(epochs):
            # Loop each sample in the training set
            for j in range(len(trainingData)):
                # Calculate activations (yj) for each layer and capture the eligibility traces (εji) required for backward propagation. (Forward pass)
                self.Classify(trainingData[j], True)

                # Calculate deltas and update weights (Backwards pass)
                # Loop each layer backwards calculating the delta (error responsibilities)
                for ind, layer in enumerate(self.layers[::-1]):
                    # If output layer then delta of each unit is  Tj - Yj (See Eq.10)
                    if layer.lType is 'output':
                        self.ComputeDeltaOutput(labelData[j], len(self.layers)-1-ind)
                        continue
                    # No deltas for input layer
                    elif layer.lType is 'input':
                        continue
                    # Loop each unit in the layer calculating it's delta TODO vectorise this?
                    for i, upstreamUnits in enumerate(layer.upstreamUnits):
                        if layer.lType is 'memOutput':
                            self.ComputeDeltaOutputGate(len(self.layers)-1-ind, i)
                        elif layer.lType is 'memCell':
                            self.ComputeDeltaMemCell(len(self.layers)-1-ind, i)
                        elif layer.lType is 'memForget':
                            self.ComputeDeltaForgetGate(len(self.layers)-1-ind, i)
                        elif layer.lType is 'memInput':
                            self.ComputeDeltaInputGate(upstreamUnits, len(self.layers)-1-ind, i)

                # Update weights of each layer
                for ind, layer in enumerate(self.layers):
                    # Ignore input as has no connections sending to the layer
                    if layer.lType is 'input':
                        continue
                    # Loop each unit in the layer updating the connection weights TODO vectorise this?
                    for i, upstreamUnits in enumerate(layer.upstreamUnits):
                        if layer.lType is 'output':
                            self.UpdateOutputWeights(ind, i)
                        elif layer.lType is 'memOutput':
                            self.UpdateMemOutputWeights(ind, i)
                        elif layer.lType is 'memCell':
                            self.UpdateMemCellWeights(ind, i)
                        elif layer.lType is 'memForget':
                            self.UpdateMemForgetWeights(ind, i)
                        elif layer.lType is 'memInput':
                            self.UpdateMemInputWeights(ind, i)

                print self.layers[len(self.layers)-1].unitActivations, " activations ", labelData[j]," output ", self.learningRate, " learnRate"
            if kb.kbhit():
                c = kb.getch()
                if ord(c) == 27: # ESC
                    print "Finished"
                    break
            # TODO add global error function (Eq. 9) to determine total error of training set
        f = open('weights/weights.txt','wb')
        pickle.dump([self.layers[i].connectionWeights for i in xrange(len(self.layers))], f)
        f.close()

    def Classify(self, testData, train, time=False):
        # Clear network values excluding connectionWeights. (This is equivalent to running the NN for the first time and clears unit states etc).
        for ind, layer in enumerate(self.layers):
            if layer.lType is not 'input':
                layer.ResetLayer()
        if time:
            ts = 0
            start_time = timeit.default_timer()
        # Loop each frame in the training instance
        for frame in range(len(testData)):
            # Loop each layer in order of depth starting with input layer and ending with output layer
            for ind, layer in enumerate(self.layers):
                # Set input layer
                if layer.lType == 'input':
                    layer.unitActivations = np.asarray(testData[frame])
                    continue

                # Remove previous activations
                layer.unitActivations = []

                # Loop each unit in the layer calculating it's activation TODO vectorise this?
                for i, upstreamUnits in enumerate(layer.upstreamUnits):
                    if layer.lType is 'memCell':
                        # Update previous unit states
                        if i is 0:
                            layer.previousUnitStates = layer.unitStates[:]
                        self.ComputeMemCellActivation(upstreamUnits, ind, i)
                    elif layer.lType is 'memInput' or layer.lType is 'memForget' or layer.lType is 'memOutput':
                        self.ComputeGateActivation(upstreamUnits, ind, i)
                    # Gate the connections coming out of the memCell
                    elif self.layers[layer.depth-2].lType is 'memCell':
                        self.ComputeMemGatedActivation(upstreamUnits, ind, i)
                    else:
                        self.ComputeUnitActivation(upstreamUnits, ind, i)

                # Cast list to numpy array
                layer.unitActivations = np.array(layer.unitActivations)
            if train is True:
                # Loop each layer calculating the elig traces
                for ind, layer in enumerate(self.layers):
                    # No elig traces for input layer
                    if layer.lType is 'input':
                        continue
                    # Loop each unit in the layer calculating it's eligibility trace for each of it's input connections TODO vectorise this?
                    for i, upstreamUnits in enumerate(layer.upstreamUnits):
                        if layer.lType is 'memCell':
                            self.ComputeMemCellTraces(upstreamUnits, ind, i)
                        # Eligibility trace of unit connections gated by output gate layer units
                        elif self.layers[layer.depth-1].lType is 'memOutput':
                            self.ComputeMemGatedTraces(upstreamUnits, ind, i)
                        # Calculate eligibility traces of normal ungated unit connections
                        else:
                            self.ComputeUnitTraces(upstreamUnits, ind, i)
                        # Compute extended elig traces for memInput and memForget
                        if layer.lType is 'memInput':
                            self.ComputeExMemInputTraces(upstreamUnits, ind, i)
                        elif layer.lType is 'memForget':
                            self.ComputeExMemForgetTraces(upstreamUnits, ind, i)
            if time:
                if ts == 29:
                    et = timeit.default_timer() - start_time
                    print "30 frames elapsed time: ", et
                ts += 1

    def GetOutput(self):
        return self.layers[len(self.layers)-1].unitActivations

