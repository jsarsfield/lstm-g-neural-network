import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import Utilities as u
from mpl_toolkits.mplot3d.art3d import juggle_axes


class AnimatedScatter(object):
    def __init__(self, skeletonData, numpoints=5):
        self.numpoints = numpoints
        #self.stream = skeletonData #self.data_stream()
        self.angle = 0
        self.fig = plt.figure()
        self.skeletonData = skeletonData
        self.ax = self.fig.add_subplot(111,projection = '3d')
        self.ani = animation.FuncAnimation(self.fig, self.update, interval=0.1,
                                           init_func=self.setup_plot, blit=True)

    def change_angle(self):
        self.angle = (self.angle + 1)%360

    def setup_plot(self):
        X = np.reshape(self.skeletonData[0],(len(self.skeletonData[0])/3,3)) #next(self.stream)
        c = ['b', 'r', 'g', 'y', 'm']
        rav = self.skeletonData.ravel()
        minXRange, maxXRange = rav[0:rav.size:3].min(), rav[0:rav.size:3].max()
        minYRange, maxYRange = rav[1:rav.size:3].min(), rav[1:rav.size:3].max()
        minZRange, maxZRange = rav[2:rav.size:3].min(), rav[2:rav.size:3].max()
        self.scat = self.ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=c, s=10, animated=True)
        self.ax.view_init(elev=270, azim=90)
        self.ax.set_xlabel("X")
        self.ax.set_ylabel("Y")
        self.ax.set_zlabel("Z")
        self.ax.set_xlim3d(minXRange, maxXRange)
        self.ax.set_ylim3d(minYRange, maxYRange)
        self.ax.set_zlim3d(minZRange, maxZRange)
        return self.scat,

    def update(self, i):
        if i >= len(self.skeletonData)-1:
            plt.close()
        data = np.reshape(self.skeletonData[i],(len(self.skeletonData[i])/3,3)) #next(self.stream)
        self.scat._offsets3d = juggle_axes(data[:,0], data[:,1], data[:,2], 'z') #( np.ma.ravel(data[:,0]) , np.ma.ravel(data[:,0]) , np.ma.ravel(data[:,0]) )
        #self.change_angle()
        #self.ax.view_init(self.angle,90)
        plt.draw()
        return self.scat,

    def show(self):
        plt.show()


def VisualiseSkeleton(skeletonData):
    skeletonData = u.ConcatenateList(skeletonData)
    indices = np.hstack((np.arange(12,21),np.arange(24,33)))
    skeletonData = np.array(skeletonData) # [:,indices]
    a = AnimatedScatter(skeletonData)
    a.show()