import numpy as np
import configparser as cp

# Load CSV data into a single 2d array.
def LoadFile(file):
    config = cp.ConfigParser()
    config.read('config.ini')
    path = config['Paths']['data_dir']

    with open(path+file, 'rb') as csvfile:
        data = np.loadtxt(csvfile, delimiter=' ')
    data=data[:,1:]
    return np.delete(data,np.arange(3,data.shape[1]+3,4),1)

# Load a sep file containing start and finish indices of a continuous stream.
# Used for separating a 2D array of multiple actions into a list of 2D arrays containing an action.
# Check Utilities.SeparateSequence function
def LoadSep(file):
    config = cp.ConfigParser()
    config.read('config.ini')
    path = config['Paths']['data_dir']

    with open(path+file, 'rb') as csvfile:
        data = np.loadtxt(csvfile,dtype=np.int)
    return data

# Load the number of tags in the tagstream
def LoadNumTags(file):
    config = cp.ConfigParser()
    config.read('config.ini')
    path = config['Paths']['data_dir']
    return sum(1 for line in open(path+file))-1